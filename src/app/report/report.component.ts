import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Calculation } from './calculation';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  calculations: Calculation[];

  constructor(private http: HttpClient) { }

  log() {
    let headers = new HttpHeaders({
   });
   let options = {
      headers: headers
   }
   //this.http.get<Calculation[]>('http://localhost:8081/reports/', { headers: headers}).subscribe(result => {
   this.http.get<Calculation[]>('https://api.etgcalculator.com/reports/', { headers: headers}).subscribe(result => {
      this.calculations = result.sort((a, b) => (a.dateTime > b.dateTime ? -1 : 1));;

      this.calculations.forEach(c => {
        c.etg = this.calculateETG(c);
      });
   
    }, error => console.error(error));
  }
  
  calculateETG(data){
    let genderalVal = 3;
    if (data.sex === 1) {
      genderalVal = 3.12;
    }

    let adjustedWeight = data.numberDrinks * (170.0/data.weight);
    let adjustedHhours = (Math.pow(2.0,((data.hoursUntilTest-3.0 + (data.hoursDrank/2))/genderalVal)));
    let adjustedResult = ((Math.pow(genderalVal, adjustedWeight)));
    let result = 4500.0 * (adjustedResult/adjustedHhours);
    return Math.round(result).toString() + ' ng/ml';
}

  ngOnInit() {

  }

}
