import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  router: Router;
  title = 'ETGCalculatorUI';
  ETGCalculatorMain = 'ETGCalculatorMain';
  

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }
  
  constructor(rtr: Router, private titleService: Title) {
    this.router = rtr;
  }

  public viewWhat() {
    this.setTitle("ETG Calculator");
    this.router.navigate(['/what']);
  }
  public viewHow() {
    this.setTitle("ETG Calculator");
    this.router.navigate(['/how']);
  }
  public viewNotes() {
    this.setTitle("ETG Calculator");
    this.router.navigate(['/notes']);
  }
  public viewDisclaimer() {
    this.setTitle("ETG Calculator");
    this.router.navigate(['/disclaimer']);
  }
  public viewHome() {
    this.setTitle("ETG Calculator");
    this.router.navigate(['/']);
  }
  public viewReferences() {
    this.setTitle("ETG Calculator");
    this.router.navigate(['/references']);
  }
  public viewAlcoholTaperGuide() {
    this.titleService.setTitle("Alcohol Taper Schedule Generator and Guide");
    this.router.navigate(['/alcoholTaperScheduleGenerator']);
  }
}
