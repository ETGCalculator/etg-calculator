import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { CalculatorComponent } from './calculator/calculator.component';
import { RouterModule } from '@angular/router';
import { WhatComponent } from './what/what.component';
import { HowComponent } from './how/how.component';
import { NotesComponent } from './notes/notes.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { MatDivider, MatDividerModule, MatIconModule, MatList, MatListItem, MatListModule } from '@angular/material';
import { ReferencesComponent } from './references/references.component';
import { DisqusModule } from "ngx-disqus";
import { AdsenseModule } from 'ng2-adsense';
import {MatRadioModule} from '@angular/material/radio';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { HttpClientModule } from '@angular/common/http';
import { ReportComponent } from './report/report.component';
import { AlcoholTaperGuideComponent } from './alcohol-taper-guide/alcohol-taper-guide.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    WhatComponent,
    HowComponent,
    NotesComponent,
    DisclaimerComponent,
    ReferencesComponent,
    ReportComponent,
    AlcoholTaperGuideComponent
  ],
  imports: [
    BrowserModule, FormsModule, BrowserAnimationsModule, AppRoutingModule, MatListModule, HttpClientModule, NgxMaterialTimepickerModule,
    NgxEchartsModule.forRoot({
      echarts,
    }),
     MatListModule, MatDividerModule,MatIconModule, MatRadioModule,
     
    DisqusModule.forRoot('etgcalculator'),  AdsenseModule.forRoot({
      adClient: 'ca-pub-4153230998283248',
      adSlot: 9974461706,
    }),
  ],
  entryComponents: [CalculatorComponent],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
