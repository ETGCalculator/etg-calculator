import { Component, OnInit } from '@angular/core';
import { MatDivider, MatList} from '@angular/material';
import { graphic } from 'echarts';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute) {
    const options2 = {
      responseType: 'text' as const,
    };
    this.http.get('https://www.cloudflare.com/cdn-cgi/trace', options2).subscribe(result => {
      var lines = result.split('\n');
        for(var i = 0;i < lines.length;i++){
          if(lines[i].indexOf('ip=') > -1) {
            this.ip = lines[i].replace('ip=', '');
          }
          
        }
    }, error => console.error(error));
   }
  options: any;
  theme = 'dark';
  ip = '';
  hideCalc = false;
  ETGResult ='Enter values and click calculate.';
  numberOfDrinks = 6.0;
  hours=24.0;
  weight=180.0;
  hideRed = true;
  hideYellow = true; 
  hidePass = true;
  hideDisclaimer = true;
  hoursDrank = 6.0;
  sex = 0;


  mainDataColor = 'rgba(30,144,255, 0.6)';
  cutoffColor = 'rgba(255, 255, 0, 0.6)';
  cutoffHourColor = 'rgba(0,100,0,1.0)';

  onSexChanged(entry): void {
    this.sex = entry;
  }

  loadChart() {
    const mainData = [];
    const standardCutOff = [];
    
    let standardCutOffHour = 0;
    let lowCutOffHour = 0;
    let checkStandard = true;
    let checkLow = true;

    let highValue = -1;

    var hoursToCheck = Array.from(Array(90).keys());

    let checkStartZoom = true;
    let checkEndZoom = true;

    let startZoom = 0;
    let endZoom = 90;

    hoursToCheck.forEach(h=> {
      
      let genderalVal = 3;
      if (this.sex === 1) {
        genderalVal = 3.12;
      }
  
      let adjustedWeight = this.numberOfDrinks*(170.0/this.weight);
      let adjustedHhours = (Math.pow(2.0,((h-3.0 + (this.hoursDrank/2))/genderalVal)));
      let adjustedResult = ((Math.pow(genderalVal, adjustedWeight)));
      let result = 4500.0 * (adjustedResult/adjustedHhours);
      //this.ETGResult = Math.round(result).toString() + ' ng/ml';
      
      const mainArrayDataItem = [h, Math.round(result)];
      const standardCutoffDataItem = [h, 500];
      mainData.push(mainArrayDataItem);
      standardCutOff.push(standardCutoffDataItem);

  
      if(checkStartZoom) {
        if(Math.round(result)<2000) {
          startZoom = h;
          checkStartZoom = false;

          if(highValue === -1) {
            //highValue = Math.round(result * 0.1);
            highValue = Math.round(result);
          }
        }
      }

      if(checkEndZoom) {
        if(Math.round(result)<51) {
          endZoom = h;
          checkEndZoom = false;
        }
      }
 
      if(checkStandard) {
        if(Math.round(result)< 500){
          checkStandard = false;
          standardCutOffHour = h;
        }
      }

      if(checkLow) {
        if(Math.round(result)< 500){
          checkLow = false;
          lowCutOffHour = h;
        }
      }

    });

  
    var colors = ['#5470C6', '#91CC75', '#EE6666'];

    this.options = {
      dataZoom : {
        show : true,
        type: "inside",
        realtime: true,
        start : startZoom,
        end : endZoom
      },
      grid: {
        containLabel : true,
        width: '250px'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
          }
      },
      toolbox: {
        feature: {dataZoom: {
            yAxisIndex: 'none'
        },
            saveAsImage: {show: true}
        }
      },
      legend: {
        data: ['ETG Levels Over Time', 'Cutoff Level', '500mg/ml cutoff'],
        x: 'left',
        y: 'bottom',
        show: true
      },
      title: {
        text: 'ETG by Hour',
        x: 'center'
      },
      xAxis: {
        silent: false,
        splitLine: {
          show: false,
        },
        //type: 'time',
        name: 'Date',
        nameLocation: 'middle',
        nameGap: 50,
        axisLine: {
          show: true,
          lineStyle: {
              color: colors[0]
          }
        },
      },
      yAxis: {       
        //name: 'Current Level',
        nameLocation: 'middle',
        nameGap: 50,
        axisLine: {
          show: true,
          lineStyle: {
              color: colors[1]
          }
        },
      },
      series: [
        {
          name: 'ETG Levels Over Time',
          type: 'bar',
          data: mainData,
          itemStyle: {
            color: new graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: this.mainDataColor
              },
              {
                offset: 1,
                color: this.mainDataColor
              }
            ])
          },
          animationDelay: (idx) => idx * 10
        },
        {
          name: 'Cutoff Level',
          type: 'line',
          data: standardCutOff,
          itemStyle: {
            color: new graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: this.cutoffColor
              },
              {
                offset: 1,
                color: this.cutoffColor
              }
            ])
          },
          animationDelay: (idx) => idx * 10 + 100,
        },
        {
          name: '500mg/ml cutoff: ' + standardCutOffHour,
          type: 'line',
          data: [],
          itemStyle: {
            color: new graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: this.cutoffHourColor
              },
              {
                offset: 1,
                color: this.cutoffHourColor
              }
            ])
          },
          markLine: {
            label:{
              offset:[-62, 40],
              align: 'left'
            },
            data: [   
                    [
                      {name: '500mg/ml cutoff hour: ' + standardCutOffHour, xAxis: standardCutOffHour, yAxis: 0},
                      {name: '500mg/ml cutoff hour: ' + standardCutOffHour, xAxis: standardCutOffHour, yAxis: highValue},
                    ]
                  ]
          }
        },
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
  }

  log() {
    let headers = new HttpHeaders({
   });
   let options = {
      headers: headers
   }
    this.http.get<any>('https://api.etgcalculator.com/calculator/'
    //this.http.get<any>('http://localhost:8081/calculator/'
    + this.numberOfDrinks + '/' + this.hours + '/' +  this.weight + '/' +  this.sex + '/' +  this.hoursDrank + '/' + this.ip, { headers: headers}).subscribe(result => {
      console.log('result: ');
      console.log(result);
    }, error => console.error(error));
  }

  calculate() {
    if(
      !this.isNumber(this.numberOfDrinks) ||
      !this.isNumber(this.hours) ||
      !this.isNumber(this.hoursDrank) ||
      !this.isNumber(this.weight)
      ) {
        this.ETGResult ='Please enter a numerical value for all fields.';
        this.hideDisclaimer = true;
        return;
    }
      this.log();
      if(
        this.numberOfDrinks <= 0 ||
        this.hours <= 0 ||
        this.hoursDrank <= 0 ||
        this.weight <= 0
        ) {
          this.ETGResult ='Please enter a value greater than 0 for all fields.';
          this.hideDisclaimer = true;
          return;
    }

    let genderalVal = 3;
    if (this.sex === 1) {
      genderalVal = 3.12;
    }

    let adjustedWeight = this.numberOfDrinks*(170.0/this.weight);
    let adjustedHhours = (Math.pow(2.0,((this.hours-3.0 + (this.hoursDrank/2))/genderalVal)));
    let adjustedResult = ((Math.pow(genderalVal, adjustedWeight)));
    let result = 4500.0 * (adjustedResult/adjustedHhours);
    this.ETGResult = Math.round(result).toString() + ' ng/ml';
    
    this.hideCalc = true;

    if(result > 500) {
      this.hideRed = false;
      this.hideYellow = true;
      this.hidePass = true;
    } else if (result > 250) {
      this.hideRed = true;
      this.hideYellow = false;
      this.hidePass = true;
    } else {
      this.hideRed = true;
      this.hideYellow = true;
      this.hidePass = false;
    }
    this.hideDisclaimer = false;
    this.loadChart();
  }

  isNumber(value: string | number): boolean
  {
    return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
  }
  ngOnInit() {
    let numberOfDrinks = this.activatedRoute.snapshot.params.numberOfDrinks;
    console.log('number drinks: ' + numberOfDrinks);// OUTPUT 1534
    let weight = this.activatedRoute.snapshot.params.weight;
    console.log('weight: ' + weight);// OUTPUT red

  }
}
