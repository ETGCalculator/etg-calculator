import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CalculatorComponent } from './calculator/calculator.component';
import { HowComponent } from './how/how.component';
import { NotesComponent } from './notes/notes.component';
import { WhatComponent } from './what/what.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { ReferencesComponent } from './references/references.component';
import { ReportComponent } from './report/report.component';
import { AlcoholTaperGuideComponent } from './alcohol-taper-guide/alcohol-taper-guide.component';

const routes: Routes = [
  {
   path: '',
   redirectTo: 'main',
   pathMatch: 'full'
 },
 {
   path: 'main',
   component: CalculatorComponent
 },
 {
   path: 'etg-levels-calculated-detail/:numberOfDrinks/:weight',
   component: CalculatorComponent
 },
 {
   path: 'calculator',
   component: CalculatorComponent
 },
 {
   path: 'how',
   component: HowComponent
 },
 {
   path: 'reports',
   component: ReportComponent
 },
 {
   path: 'notes',
   component: NotesComponent
 },
 {
   path: 'what',
   component: WhatComponent
 },
 {
   path: 'disclaimer',
   component: DisclaimerComponent
 },
 {
   path: 'references',
   component: ReferencesComponent
 },
 {
   path: 'alcoholTaperGuide',
   component: AlcoholTaperGuideComponent
 },
 
 {
   path: 'alcoholTaperScheduleGenerator',
   component: AlcoholTaperGuideComponent
 }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
