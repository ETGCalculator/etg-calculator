
import { Component, OnInit } from '@angular/core';
import { TaperByAmount } from '../entities/TaperByAmount';
import { TaperByAmountDetail } from '../entities/TaperByAmountDetail';
import { TaperByAmountDetailSpecificDrink } from '../entities/TaperByAmountDetailSpecificDrink';
import { TaperByVolume } from '../entities/TaperByVolume';
import { TaperByVolumeDetail } from '../entities/TaperByVolumeDetail';
import { TaperByVolumeDetailSpecificDrink } from '../entities/TaperByVolumeDetailSpecificDrink';
import html2canvas from 'html2canvas';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-alcohol-taper-guide',
  templateUrl: './alcohol-taper-guide.component.html',
  styleUrls: ['./alcohol-taper-guide.component.css']
})
export class AlcoholTaperGuideComponent implements OnInit {
  hideUnits = true;
  taperType = 0;
  units = 0;
  speed = 1;
  taperFactor = 0.90;
  consecutiveDays = 1;

  startPicker: any = '5:00 PM';
  endPicker: any = '1:00 AM';
  amount = 13;
  unitText = 'oz';

  alreadyEdited = false;

  TaperByVolumeVM: TaperByVolume = null;
  TaperByAmountVM: TaperByAmount = null;

  constructor(private http: HttpClient, private titleService: Title) {
    this.titleService.setTitle("Alcohol Taper Schedule Generator and Guide");
   }

  ngOnInit() {
  }

  onTaperTypeChanged(entry): void {
    this.taperType = entry;
    if (this.taperType == 1){
      this.hideUnits = false;
      if (this.units == 0 && !this.alreadyEdited){
        this.amount = 40;
        this.alreadyEdited = true;
      }
    } else {
      this.hideUnits = true;
    }
  }

  onUnitsChanged(entry): void {
    this.units = entry;
    if(entry == 0) {
      this.unitText = 'oz';
    } else {
      this.unitText = 'ml';
    }
    this.logInfo();
  }

  onTaperSpeedChanged(entry): void {
    this.speed = entry;

    if (this.speed == 0) {
      this.taperFactor = 0.85;
      this.consecutiveDays = 0;
    } else if (this.speed == 1) {
      this.taperFactor = 0.90;
      this.consecutiveDays = 1;
    } else if (this.speed == 2) {
      this.taperFactor = 0.95;
      this.consecutiveDays = 2;
    }


    this.logInfo();
  }

  setStartTime(event): void {
    this.startPicker = event;
    this.logInfo();
  }

  setEndTime(event): void {
    this.endPicker = event;
    this.logInfo();
  }

  roundNearestFifty(num){
    return Math.floor(num / 50)*50;
    }

  roundTimeQuarterHour(originTime) {
    var minutesToRound = 15;
    let [hours, minutes] = originTime.split(':');
    hours = parseInt(hours);
    minutes = parseInt(minutes);

    // Convert hours and minutes to time in minutes
    var time = (hours * 60) + minutes; 

    let rounded = Math.round(time / minutesToRound) * minutesToRound;
    let rHr = ''+Math.floor(rounded / 60)
    let rMin = ''+ rounded % 60

    var result = rHr.padStart(2, '0')+':'+rMin.padStart(2, '0')

    if(originTime.indexOf('AM') > -1){
      result = result + ' AM';
    } else {
      result = result + ' PM';
    }

    return result;
}


  logInfo() {
    console.log("taper Type: " + this.taperType);
    console.log("units " + this.units);
    console.log("speed: " + this.speed);
    console.log("startPicker: " + this.startPicker);
    console.log("endPicker: " + this.endPicker);
    console.log("Rounded StartTime: " + this.roundTimeQuarterHour(this.startPicker));
    console.log("Rounded EndTime: " + this.roundTimeQuarterHour(this.endPicker));

    var timeStartString = "01/01/2007 " + this.roundTimeQuarterHour(this.startPicker);
    var timeEndString ="01/01/2007 " + this.roundTimeQuarterHour(this.endPicker);
    
    if(timeStartString.indexOf('PM') > -1 && timeEndString.indexOf('AM') > -1 ){
      timeEndString ="01/02/2007 " + this.roundTimeQuarterHour(this.endPicker);
    }
 

    var timeStart: any = new Date(timeStartString);
    var timeEnd: any = new Date(timeEndString);

    var diff: number = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds

    var minutes = diff % 60;
    var hours = (diff - minutes) / 60;

    if(hours < 0) {
      hours = hours + 24;
    }

    console.log("Time Diff: " + hours + ":" + minutes);
  }

  async printThis() {
    var el = document.getElementById('taperPlanWrapperAmount');

    if(this.TaperByVolumeVM != null) {
      el = document.getElementById('taperPlanWrapperVolume');
    }

    const options = {
      type: "dataURL",
    };
    //const printCanvas = await html2canvas(el);

    html2canvas(el).then(canvas => {
      const link = document.createElement("a");
      link.setAttribute("download", "download.png");
      link.setAttribute(
        "href",
        canvas
          .toDataURL("image/png")
          .replace("image/png", "image/octet-stream")
      );
      link.click();
  
      console.log("done");
    });

/*
    const link = document.createElement("a");
    link.setAttribute("download", "download.png");
    link.setAttribute(
      "href",
      printCanvas
        .toDataURL("image/png")
        .replace("image/png", "image/octet-stream")
    );
    link.click();

    console.log("done");
    */
  }


  log() {
    let headers = new HttpHeaders({
   });
   let options = {
      headers: headers
   }
    this.http.get<any>('https://api.etgcalculator.com/taper/'
    + this.taperType + '/' + this.amount + '/' +  this.units + '/' +  this.speed , { headers: headers}).subscribe(result => {
      console.log('result: ');
      console.log(result);
    }, error => console.error(error));
  }

  calculate() {
    var ozToMl = 29.573;

    this.TaperByVolumeVM = null;
    this.TaperByAmountVM = null;

    var drinksByDayNonReduced : Array <number> = [];
    var drinksByDayRounded : Array <number> = [];
    var drinksByDayConsolidated : Array <number> = [];

    var amountForCalc = this.amount;

    if(this.taperType == 1 && this.units == 0) {
      amountForCalc = this.amount * ozToMl;
    }

    var startingNumberOfDrinksPerDay = Math.ceil(amountForCalc);
    drinksByDayNonReduced.push(startingNumberOfDrinksPerDay);
    
    var nextDayNumber = this.taperFactor * startingNumberOfDrinksPerDay;
    drinksByDayNonReduced.push(nextDayNumber);

    while (nextDayNumber > 1) {
      nextDayNumber = this.taperFactor * nextDayNumber;
      drinksByDayNonReduced.push(nextDayNumber);
    }
    
    for (var i = 0; i < drinksByDayNonReduced.length; i++) {
      if(this.taperType == 0){
        drinksByDayRounded.push(Math.ceil(drinksByDayNonReduced[i]));
      } else {

        if(drinksByDayRounded[drinksByDayRounded.length - 1] - this.roundNearestFifty(drinksByDayNonReduced[i]) >= 150) {
          drinksByDayRounded.push(drinksByDayRounded[drinksByDayRounded.length - 1] - 150);
        } else {
          drinksByDayRounded.push(this.roundNearestFifty(drinksByDayNonReduced[i]));

        }
      }
    }

    for (var i = 0; i < drinksByDayRounded.length; i++) {
      if(drinksByDayConsolidated.length > this.consecutiveDays) {

        if(drinksByDayConsolidated[drinksByDayConsolidated.length - 1 - this.consecutiveDays] != drinksByDayRounded[i]){
          drinksByDayConsolidated.push(drinksByDayRounded[i]);
        }
        
        
      } else {
        drinksByDayConsolidated.push(drinksByDayRounded[i]);
      }
    }

    if(this.taperType == 0){
      drinksByDayConsolidated.push(1);
      drinksByDayConsolidated.push(0.5);
      drinksByDayConsolidated.push(0.5);
    } else {
      //drinksByDayConsolidated.push(100);
      //drinksByDayConsolidated.push(50);
      //drinksByDayConsolidated.push(50);
    }

    while(drinksByDayConsolidated[drinksByDayConsolidated.length-1] == 0 ) {
      drinksByDayConsolidated.pop();
    }

    var timeStartString = "01/01/2007 " + this.roundTimeQuarterHour(this.startPicker);
    var timeEndString ="01/01/2007 " + this.roundTimeQuarterHour(this.endPicker);
    if(timeStartString.indexOf('PM') > -1 && timeEndString.indexOf('AM') > -1 ){
      timeEndString ="01/02/2007 " + this.roundTimeQuarterHour(this.endPicker);
    }
 

    var timeStart: any = new Date(timeStartString);
    var timeEnd: any = new Date(timeEndString);

    var diff: number = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds

    var minutes = diff % 60;
    var hours = (diff - minutes) / 60;
    
    if(hours < 0) {
      hours = hours + 24;
    }

    if (minutes > 30) {
      hours = hours + 1;
    }

    if(this.taperType == 0){
      var hoursPerDrink = hours/Math.ceil(this.amount);
      var minutesPerDrink = Math.ceil(hoursPerDrink*60);

      var taperByAmount = new TaperByAmount(drinksByDayConsolidated.length);

      for (var i = 0; i < drinksByDayConsolidated.length; i++) {
        var taperByAmountDetail = new TaperByAmountDetail(i, drinksByDayConsolidated[i]);
        var timeStartCalc = timeStart;

        for (var j = 0; j < drinksByDayConsolidated[i]; j++) {
          var specificDrinkByAmount =  new TaperByAmountDetailSpecificDrink(timeStartCalc, j);
          taperByAmountDetail.SpecificDrinks.push(specificDrinkByAmount);
          timeStartCalc = new Date(timeStartCalc.getTime() + minutesPerDrink*60000);
        }

        taperByAmount.TaperByAmountDetails.push(taperByAmountDetail);
      }
      console.log(taperByAmount);
      this.TaperByAmountVM = taperByAmount;
    } else {
      var taperByVolume = new TaperByVolume(drinksByDayConsolidated.length);


      for (var i = 0; i < drinksByDayConsolidated.length; i++) {
        var amountPerHour = Math.ceil(drinksByDayConsolidated[i]/hours);
        var timeStartCalc = timeStart;

        var amountAllowed = drinksByDayConsolidated[i];

        if (this.units == 0) {
          amountAllowed = Math.floor(amountAllowed/ozToMl)
        }

        var taperDetail = new TaperByVolumeDetail(i, amountAllowed);

        if(amountPerHour > 100){
          for (var j = 0; j < hours*2; j ++){
            timeStartCalc = new Date(timeStartCalc.getTime() + 45*60000);

            var amountPerHourCalc = amountPerHour/2;

            if (this.units == 0) {
              amountPerHourCalc = (Math.round((amountPerHourCalc / ozToMl) * 10) / 10);     
            }

            var specificDrink = new TaperByVolumeDetailSpecificDrink(timeStartCalc, Math.round(amountPerHourCalc), j);
            taperDetail.SpecificDrinks.push(specificDrink);
          }
        } else {
          var amountLeft = drinksByDayConsolidated[i];

          var exitLoop = false;
          var dayIndex = 0;
          var cumlativeTotal = 0;
          while (!exitLoop) {
            var amountToConsume = 50;
            amountLeft = amountLeft - amountToConsume;

            if(amountLeft < 0) {
              amountToConsume = amountToConsume + amountLeft;
              exitLoop = true;
              if (this.units == 0) {
                taperDetail.AmountAllowed = Math.round(10*cumlativeTotal)/10;
              }
            } else {
              timeStartCalc = new Date(timeStartCalc.getTime() + 40*60000);

              if (this.units == 0) {
                amountToConsume = Math.round(10 * amountToConsume / ozToMl)/10;
                cumlativeTotal = cumlativeTotal + amountToConsume;
              }

              var specificDrink = new TaperByVolumeDetailSpecificDrink(timeStartCalc, amountToConsume, dayIndex);
              taperDetail.SpecificDrinks.push(specificDrink);
            }
            dayIndex++;
          }
        }
        taperByVolume.TaperByVolumeDetails.push(taperDetail);
      }
      console.log(taperByVolume);
      this.TaperByVolumeVM = taperByVolume;
    }
    this.log();
  }
}
