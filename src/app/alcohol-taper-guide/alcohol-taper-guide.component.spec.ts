import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlcoholTaperGuideComponent } from './alcohol-taper-guide.component';

describe('AlcoholTaperGuideComponent', () => {
  let component: AlcoholTaperGuideComponent;
  let fixture: ComponentFixture<AlcoholTaperGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlcoholTaperGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlcoholTaperGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
