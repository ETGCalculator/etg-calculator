import { TaperByVolumeDetail } from "./TaperByVolumeDetail";

export class TaperByVolume {
    NumberOfDays: number;
    TaperByVolumeDetails: Array<TaperByVolumeDetail>;

    constructor(numberOfDays: number) {
        this.TaperByVolumeDetails = [];
        this.NumberOfDays = numberOfDays;
    }
}