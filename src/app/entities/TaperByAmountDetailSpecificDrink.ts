export class TaperByAmountDetailSpecificDrink{
    Time: string;
    DrinkIndex: number;
    constructor(time: string, drinkIndex: number) {
        this.Time = time;
        this.DrinkIndex = drinkIndex;
    }
}