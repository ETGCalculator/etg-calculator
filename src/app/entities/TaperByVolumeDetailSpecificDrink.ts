export class TaperByVolumeDetailSpecificDrink {
    Time: string;
    Amount: number;
    DrinkIndex: number;

    constructor(time: string, amount: number, drinkIndex: number) {
        this.Time = time;
        this.Amount = amount;
        this.DrinkIndex = drinkIndex;
    }
}