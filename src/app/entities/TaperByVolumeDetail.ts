import { TaperByVolumeDetailSpecificDrink } from "./TaperByVolumeDetailSpecificDrink";

export class TaperByVolumeDetail {
    DayIndex: number;
    AmountAllowed: number;
    SpecificDrinks: Array<TaperByVolumeDetailSpecificDrink>;

    constructor(dayIndex: number, AmountAllowed: number) {
        this.DayIndex = dayIndex;
        this.AmountAllowed = AmountAllowed;
        this.SpecificDrinks = [];
    }
}