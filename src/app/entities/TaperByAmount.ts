import { TaperByAmountDetail } from "./TaperByAmountDetail";

export class TaperByAmount {

    NumberOfDays: number;
    TaperByAmountDetails: Array<TaperByAmountDetail>;

    constructor(numberOfDays: number) {
        this.TaperByAmountDetails = [];
        this.NumberOfDays = numberOfDays;
    }
}