import { TaperByAmountDetailSpecificDrink } from "./TaperByAmountDetailSpecificDrink";

export class TaperByAmountDetail{
    DayIndex: number;
    NumberDrinksAllowed: number;
    SpecificDrinks: Array<TaperByAmountDetailSpecificDrink>;

    constructor(dayIndex: number, numberDrinksAllowed: number) {
        this.DayIndex = dayIndex;
        this.NumberDrinksAllowed = numberDrinksAllowed;
        this.SpecificDrinks = [];
    }
}