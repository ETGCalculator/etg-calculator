# ETGCalculator.com
Project repo for the [ETG Calculator](http://www.ETGCalculator.com) which calculates the amount of ETG in your Urine/Blood used in drug tests that measure your ETG Level to detect for consumption of alcohol

Twitter: http://twitter.com/etgcalculator

Weebly Blog: [ETG Alcohol Guide](https://etgcalculator.weebly.com/)